#pragma once
#define ENABLE_CUDA
#include <psrdada_cpp/dadaflow/processors/MkrecvAlveoPFB64Parser.hpp>
#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cuda_utils.hpp>
#include <psrdada_cpp/dadaflow/CudaStream.cuh>
#include <psrdada_cpp/dadaflow/DescribedData.hpp>
#include <psrdada_cpp/dadaflow/units.hpp>
#include <psrdada_cpp/dadaflow/utils.hpp>
#include <psrdada_cpp/dadaflow/vector_traits.cuh>

#include <cooperative_groups.h>
#include <cufftdx.hpp>

using namespace cufftdx;
namespace cg = cooperative_groups;
namespace psr = psrdada_cpp;

namespace edd_transient
{
namespace kernels
{

// cufftDx kernel
// TFTP input
// Each block loads TTP data in PT oder
// TT length = nchans * naccumulate (typically 16 and 150 to 200)

template <typename T>
    requires Vec2<T>
__host__ __device__ float complex_magnitude(T const& val)
{
    return val.x * val.x + val.y * val.y;
}

/**

// Keeping this as an idea for a future performance upgrade
// Here we would use a cufftdx::Thread rather than a cufftdx::Block FFT.
// Data would be loaded coalesced and then transposed into
// the correct ordering in each thread.
// Each thread then does a small FFT (2-32 point) for each
// polarisation before performing a warp reduction across
// a block of 32 FFTs. The output is then stored or atomicAdded
// to an output buffer.
// Further optimisation could be acheived by using __half2 as the
// complex type such that only 1 FFT is executed instead of two.
// Also the final warp shfl reduction could reuse the load_buffer
// so that the final write back to global could be coalesced

template <int tile_sz>
__device__ int reduce_sum_tile_shfl(thread_block_tile<tile_sz> g, float val)
{
    for (int i = g.size() / 2; i > 0; i /= 2) {
        val += g.shfl_down(val, i);
    }
    return val;
}

__global__ fft_detector_accumulator2()
{
    using complex_type = FFT::value_type;
    InputType load_buffer[FFT::input_length];
    complex_type fft_buffer[Npol][FFT::input_length]

    // On load we now work in a tile of size 32
    cg::thread_block block = cg::this_thread_block();
    cg::thread_block_tile<32> warp = cg::tiled_partition<32>(block);
    const unsigned tid = block.thread_rank();
    const unsigned lane_id = warp.thread_rank();
    const unsigned warp_id = warp.meta_group_rank();

    // Each thread loads FFT::input_length samples
    // In tiles of FFT::input_length threads
    cg::thread_block_tile<FFT::input_length> loading_group =
cg::tiled_partition<FFT::input_length>(block); const unsigned loading_group_id =
warp.meta_group_rank(); for (int ii = 0; ii < loading_group.size(); ++ii) {
        // Calculate input index
        load_buffer[ii] = 0;//;
    }
    loading_group.sync();

    for (int ii = 0; ii < loading_group.size(); ++ii) {
        // Calculate input index
        InputType val =
loading_group.shfl(load_buffer[loading_group.thread_rank()], ii);
        fft_buffer[0][ii].x = val.x;
        fft_buffer[0][ii].y = val.y;
        fft_buffer[1][ii].x = val.z;
        fft_buffer[1][ii].y = val.w;
    }
    loading_group.sync();

    FFT().execute(fft_buffer[0]);
    FFT().execute(fft_buffer[1]);

    for (int ii = 0; ii < FFT::input_length; ++ii) {
        float sum = 0.0f;
        sum += complex_magnitude(fft_buffer[0][ii]);
        sum += complex_magnitude(fft_buffer[1][ii]);
        sum = reduce_sum_tile_shfl<32>(warp, sum);
        if (warp.thread_rank() == 0) {
            // Write the sum to the output
        }
    }
}
*/

template <typename FFT, typename InputType, typename OutputType, int Npol = 2>
    requires Vec4<InputType>
__global__ void fft_detector_accumulator(InputType const* input,
                                         OutputType* output,
                                         int inner_t_samples,
                                         unsigned int tscrunch)
{
    using complex_type = FFT::value_type;
    static_assert(FFT::ffts_per_block % Npol == 0);
    static_assert(Npol == 2);

    // Shared memory for data loading and FFT exection
    __shared__ __align__(alignof(float4))
        complex_type fft_mem[Npol][FFT::ffts_per_block][FFT::input_length];
    complex_type* pol_0 = &(fft_mem[0][0][0]);
    complex_type* pol_1 = &(fft_mem[1][0][0]);

    // Shared memory for block reductions after FFT
    __shared__ __align__(alignof(
        float4)) float reduction_mem[FFT::ffts_per_block][FFT::input_length];

    // If complex_type is __half2 then there is an intrinsic double FFT
    // execution and data must be stored as r0,r1,i0,i1,r2,r3,i2,i3...

    // The number of time samples that will be processed by each block factoring
    // in the number of polarisations
    const int samples_per_block = FFT::ffts_per_block * FFT::input_length;

    // Each block in the y-dimension will handle a different coarse frequency
    // channel
    const int coarse_channel_idx = blockIdx.y;
    const int ncoarse_channels   = gridDim.y;

    // Each block in the x-dimension will handle a different window in time
    const int start_sample_idx = blockIdx.x * samples_per_block;
    const int ftp              = ncoarse_channels * inner_t_samples;

    cg::thread_block block = cg::this_thread_block();
    const unsigned tid     = block.thread_rank();

    for(int idx = tid; idx < samples_per_block; idx += block.size()) {
        const int sample_idx = start_sample_idx + idx;
        const int outer_t    = sample_idx / inner_t_samples;
        const int inner_t    = sample_idx % inner_t_samples;
        const int load_idx =
            outer_t * ftp + coarse_channel_idx * inner_t_samples + inner_t;
        InputType val      = input[load_idx];
        (*(pol_0 + idx)).x = val.x;
        (*(pol_0 + idx)).y = val.y;
        (*(pol_1 + idx)).x = val.z;
        (*(pol_1 + idx)).y = val.w;
    }
    block.sync();

    // --- Step 1: Execute the FFT for each polarisation ---
    FFT().execute(pol_0);
    FFT().execute(pol_1);

    // --- Step 1: Detect and accumulate ---
    // Make a pass over the block to merge the polarisations and
    // populate the reduction block (incl. FFT shift for channel ordering)
    for(int jj = 0; jj < FFT::input_length; ++jj) {
        // reorder jj?
        float sum = 0.0f;
        sum += complex_magnitude(fft_mem[0][threadIdx.y][jj]);
        sum += complex_magnitude(fft_mem[1][threadIdx.y][jj]);
        reduction_mem[threadIdx.y]
                     [(jj + FFT::input_length / 2) % FFT::input_length] = sum;
    }
    block.sync();

    // Log 2 reduction over threads
    // Could move this to a two stage reduction with
    // warps in thread_block_tile<32> with shuffles
    for(int jj = 0; jj < FFT::input_length; ++jj) {
#pragma unroll
        for(unsigned int stride = FFT::ffts_per_block / 2; stride > 0;
            stride >>= 1) {
            if(threadIdx.y < stride) {
                reduction_mem[threadIdx.y][jj] +=
                    reduction_mem[threadIdx.y + stride][jj];
            }
            block.sync();
        }
    }

    if(tid < FFT::input_length) {
        // Output is in TFf order where F is coarse channel and f is fine
        // channel
        const unsigned store_idx =
            (blockIdx.x / tscrunch) * ncoarse_channels * FFT::input_length +
            coarse_channel_idx * FFT::input_length + tid;
        atomicAdd_system(&output[store_idx], reduction_mem[0][tid]);
    }
}

} // namespace kernels

template <typename Container>
using FilterbankData =
    psr::DescribedData<Container, psr::TimeDimension, psr::FrequencyDimension>;

template <int nchans,
          int naccumulate,
          typename InputType,
          typename OutputType,
          typename Allocator>
class FFTDetectorAccumulator;

template <int nchans,
          int naccumulate,
          typename InputContainer,
          typename OutputContainer,
          typename Allocator>
    requires(Vec2<typename InputContainer::value_type> &&
             psr::is_device_container_v<InputContainer> &&
             psr::is_device_container_v<OutputContainer>)
class FFTDetectorAccumulator<nchans,
                             naccumulate,
                             psr::Alveo64PFBData<InputContainer>,
                             FilterbankData<OutputContainer>,
                             Allocator>
{
  public:
    using InputType   = psr::Alveo64PFBData<InputContainer>;
    using InputPtr    = std::shared_ptr<InputType>;
    using OutputType  = FilterbankData<OutputContainer>;
    using OutputPtr   = std::shared_ptr<OutputType>;
    using OutputTuple = std::tuple<OutputPtr>;
    static_assert(nchans % 2 == 0, "Nchans must be a power of two");
    static_assert(naccumulate % 2 == 0, "Naccumulate must be a power of two");
    using FFT =
        decltype(Size<nchans>() + Precision<float>() + Type<fft_type::c2c>() +
                 Direction<fft_direction::forward>() + SM<890>() +
                 FFTsPerBlock<naccumulate>() + Block());

    FFTDetectorAccumulator(unsigned int tscrunch, Allocator& allocator)
        : _tscrunch(tscrunch), _allocator(allocator)
    {
    }

    explicit FFTDetectorAccumulator(Allocator& allocator)
        : _tscrunch(1), _allocator(allocator)
    {
    }

    FFTDetectorAccumulator(FFTDetectorAccumulator const&)            = delete;
    FFTDetectorAccumulator& operator=(FFTDetectorAccumulator const&) = delete;
    FFTDetectorAccumulator(FFTDetectorAccumulator&&)                 = default;

    ~FFTDetectorAccumulator() {}

    OutputTuple operator()(InputPtr input_ptr)
    {
        InputType const& input         = *input_ptr;
        std::size_t const nheaps       = input.psr::TimeDimensionN<0>::nsamples();
        unsigned const inner_t_size    = input.psr::TimeDimensionN<1>::nsamples();
        std::size_t const total_t_size = nheaps * inner_t_size;
        std::size_t const total_accumulation =
            FFT::ffts_per_block * FFT::input_length * _tscrunch;
        if(inner_t_size % 2 != 0) {
            throw std::runtime_error(
                "FFTDetectorAccumulator requires the inner time "
                "dimension the PFB data to be a power of two");
        }
        if(total_t_size % total_accumulation != 0) {
            throw std::runtime_error(
                "FFTDetectorAccumulator requires the input data to"
                " contain a whole number of accumulation windows");
        }
        const std::size_t output_t_size = total_t_size / total_accumulation;
        BOOST_LOG_TRIVIAL(debug)
            << "Number of output samples: " << output_t_size;
        if(output_t_size == 0) {
            throw std::runtime_error(
                "Input size is not large enough for the given configuration");
        }
        const std::size_t output_f_size = input.nchannels() * FFT::input_length;
        OutputPtr output_ptr            = _allocator.get();
        OutputType& output              = *output_ptr;
        output.resize({output_t_size, output_f_size});
        thrust::fill(output.begin(), output.end(), typename OutputType::value_type{});
        output.timestamp(input.timestamp());
        output.timestep(input.psr::TimeDimensionN<1>::timestep() *
                        static_cast<double>(total_accumulation));
        output.channel_bandwidth(input.channel_bandwidth() /
                                 static_cast<double>(FFT::input_length));
        output.channel_zero_freq(
            input.channel_zero_freq() -
            (static_cast<double>(FFT::input_length / 2 + 0.5) *
             output.channel_bandwidth()));
        output.metadata_ptr(input.metadata_ptr());
        // We are going to do the trick of merging the inner dimension from a
        // Vec2 to a Vec4
        using V4CastT =
            vector_type_t<value_type_t<typename InputContainer::value_type>, 4>;
        dim3 grid(total_t_size / (FFT::ffts_per_block * FFT::input_length),
                  input.nchannels());
        BOOST_LOG_TRIVIAL(debug) << "Tscrunch: " << _tscrunch;
        BOOST_LOG_TRIVIAL(debug) << "Grid: " << grid;
        BOOST_LOG_TRIVIAL(debug) << "Block: " << FFT::block_dim;
        BOOST_LOG_TRIVIAL(debug)
            << "Shared mem size: " << FFT::shared_memory_size;
        BOOST_LOG_TRIVIAL(debug) << "Stream " << _stream.stream();
        kernels::fft_detector_accumulator<FFT><<<grid,
                                                 FFT::block_dim,
                                                 FFT::shared_memory_size,
                                                 _stream.stream()>>>(
            reinterpret_cast<V4CastT const*>(
                thrust::raw_pointer_cast(input.data())),
            thrust::raw_pointer_cast(output.data()),
            inner_t_size,
            _tscrunch);
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream.stream()));
        return std::tie(output_ptr);
    }

  private:
    unsigned int _tscrunch;
    Allocator& _allocator;
    psr::CudaStream _stream;
};

} // namespace edd_transient