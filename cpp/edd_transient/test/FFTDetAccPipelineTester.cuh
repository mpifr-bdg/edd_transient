#pragma once
#include "edd_transient/FFTDetAccPipeline.cuh"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace edd_transient {
namespace test {

template <int nchans_, int naccumulate_>
struct FFTDetAccPipelineTesterTraits
{
    static constexpr int nchans = nchans_;
    static constexpr int naccumulate = naccumulate_;
};

template <typename Traits>
class FFTDetAccPipelineTester : public ::testing::Test
{
public:
    FFTDetAccPipelineTester(){}

    ~FFTDetAccPipelineTester(){}

    void SetUp() override {};

    void TearDown() override {};
};

} // namespace test
} // namespace edd_transient