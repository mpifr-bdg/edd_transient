include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})
set(gtest_transient
    src/gtest_transient.cu
    src/FFTDetectorAccumulatorTester.cu
    src/FFTDetAccPipelineTester.cu
)

add_executable(gtest_transient ${gtest_transient} )
target_include_directories(gtest_transient PRIVATE "${EDD_TRANSIENT_INCLUDE_DIR}")
target_link_libraries(gtest_transient 
    ${DEPENDENCY_LIBRARIES}
    ${GTEST_LIBRARIES} 
)
add_test(gtest_transient gtest_transient --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
