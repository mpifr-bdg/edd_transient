#include <psrdada_cpp/dadaflow/vector_traits.cuh>
#include <thrust/fill.h>
#include <memory>
#include "edd_transient/test/FFTDetectorAccumulatorTester.cuh"

namespace psr = psrdada_cpp;

namespace edd_transient {
namespace test {

template <typename Traits>
void FFTDetectorAccumulatorTester<Traits>::populate_input(InputType& input, std::size_t nheaps, std::size_t nchans) {
    std::size_t inner_t_size = 2048;
    auto tsamp = std::chrono::duration<long double, pico>(500.0);
    input.resize({nheaps, nchans, inner_t_size, 2});
    input.channel_zero_freq(2.0e9 * hertz);
    input.channel_bandwidth((2.0e9/64.0) * hertz);
    input.timestamp(psr::Timestamp(0.0));
    input.psr::TimeDimensionN<0>::timestep(tsamp * static_cast<double>(inner_t_size));
    input.psr::TimeDimensionN<1>::timestep(tsamp);
    input.polarisations({"0", "1"});
    thrust::fill(input.begin(), input.end(), typename InputType::value_type{1, 0});
}

template <typename Traits>
void FFTDetectorAccumulatorTester<Traits>::evaluate_output(InputType& input, OutputType const& output, std::size_t nchans, std::size_t naccumulate) {
    ASSERT_EQ(output.nchannels(), std::size_t(input.nchannels() * nchans));
    std::size_t total_t_size = input.psr::TimeDimensionN<0>::nsamples() * input.psr::TimeDimensionN<1>::nsamples();
    ASSERT_EQ(output.nsamples(), std::size_t(total_t_size / (nchans * naccumulate)));
    // Expected DC value
    float expected_dc = input.npol() * naccumulate * float(nchans) * float(nchans);
    for (std::size_t t_idx = 0; t_idx < output.nsamples(); ++t_idx) {
        for (std::size_t F_idx = 0; F_idx < input.nchannels(); ++F_idx) {
            for (std::size_t f_idx = 0; f_idx < nchans; ++f_idx) {
                std::size_t idx = t_idx * output.nchannels() + F_idx * nchans + f_idx;
                if (f_idx == nchans/2) {
                    EXPECT_FLOAT_EQ(output[idx], expected_dc) << "Error at idx: " << t_idx << ", " << F_idx << ", " << f_idx;
                } else {
                    EXPECT_FLOAT_EQ(output[idx], 0.0f) << "Error at idx: " << idx;
                }
            }
        }
    }
}

typedef ::testing::Types<
    FFTDetectorAccumulatorTesterTraits<
        psr::Alveo64PFBData<thrust::device_vector<char2>>, 
        FilterbankData<thrust::device_vector<float>>
    >
> TestTypes;
TYPED_TEST_SUITE(FFTDetectorAccumulatorTester, TestTypes);


TYPED_TEST(FFTDetectorAccumulatorTester, single_channel_test)
{
    using Traits = TypeParam;
    using InputType = typename Traits::InputType;
    using OutputType = typename Traits::OutputType;
    using AllocatorType = psr::ResourcePool<OutputType>;
    auto tsamp = std::chrono::duration<long double, pico>(500.0);
    AllocatorType allocator(4);
    constexpr int nchans = 16;
    constexpr int naccumulate = 128;
    unsigned tscrunch{2};
    FFTDetectorAccumulator<nchans, naccumulate, InputType, OutputType, AllocatorType> node(tscrunch, allocator);
    auto input = std::make_shared<InputType>();
    this->populate_input(*input, 2 * tscrunch, 1);
    auto output_tuple = node(input);
    auto const& output_ptr = std::get<0>(output_tuple);
    auto const& output = *output_ptr;
    this->evaluate_output(*input, output, nchans, naccumulate * tscrunch);
}

TYPED_TEST(FFTDetectorAccumulatorTester, mutli_channel_test)
{
    using Traits = TypeParam;
    using InputType = typename Traits::InputType;
    using OutputType = typename Traits::OutputType;
    using AllocatorType = psr::ResourcePool<OutputType>;
    auto tsamp = std::chrono::duration<long double, pico>(500.0);
    AllocatorType allocator(4);
    constexpr int nchans = 8;
    constexpr int naccumulate = 64;
    unsigned tscrunch{4};
    FFTDetectorAccumulator<nchans, naccumulate, InputType, OutputType, AllocatorType> node(tscrunch, allocator);
    auto input = std::make_shared<InputType>();
    this->populate_input(*input, 8 * tscrunch, 64);
    auto output_tuple = node(input);
    auto const& output_ptr = std::get<0>(output_tuple);
    auto const& output = *output_ptr;
    this->evaluate_output(*input, output, nchans, naccumulate * tscrunch);
}

} // namespace test
} // namespace edd_transient