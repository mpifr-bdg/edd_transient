#include "edd_transient/test/FFTDetAccPipelineTester.cuh"

namespace psr = psrdada_cpp;

namespace edd_transient {
namespace test {

template <typename T>
struct TestStruct 
{
    std::span<T> data;
    std::vector<char> header;
};

template <typename T>
class TestParser
{
public:
    using OutputType = TestStruct<T>;
    using OutputPtr = std::shared_ptr<OutputType>;
    using OutputTuple = std::tuple<OutputPtr>;
    using ReleaseLifetime = std::shared_ptr<void>;

    TestParser() = default;
    TestParser(TestParser const&) = delete;
    TestParser& operator=(TestParser const&) = delete;
    TestParser(TestParser&&) = default;

    void parse_header(psr::RawBytes& header_bytes) {
        _header.purge();
        _header.read_from(header_bytes);
    }

    OutputTuple parse_data(psr::RawBytes& bytes, ReleaseLifetime releaser) {
        const std::size_t nelements = bytes.used_bytes() / sizeof(T);
        OutputPtr output_ptr = OutputPtr(new OutputType{{reinterpret_cast<T*>(bytes.ptr()), nelements}, _header.to_vector()}, 
        [releaser](OutputType* ptr){
            delete ptr;
            // Here the releaser doesn't need to be called. It is captured in the lambda
            // and when the lambda is called it will go out of scope, releasing the buffer
        });
        return std::tie(output_ptr);
    }

private:
    psr::DadaHeader _header;
};

typedef ::testing::Types<
    FFTDetAccPipelineTesterTraits<16, 128>
> TestTypes;
TYPED_TEST_SUITE(FFTDetAccPipelineTester, TestTypes);

TYPED_TEST(FFTDetAccPipelineTester, end_to_end)
{
    using Traits = TypeParam;
    const std::size_t fft_size = Traits::nchans;
    const std::size_t naccumulate = Traits::naccumulate;
    const std::size_t nheaps = 32;
    const std::size_t nchans = 1;
    const std::size_t heap_size_bytes = nchans * 2048 * 2 * sizeof(char2);
    const std::size_t input_buffer_bytes = heap_size_bytes * nheaps;
    const std::size_t output_buffer_bytes = heap_size_bytes * nheaps / (naccumulate) * sizeof(float)/sizeof(char2);
    const std::size_t header_size_bytes = 4096;

    // Set up input buffer
    psr::DadaDB input_db(16, input_buffer_bytes, 4,  header_size_bytes);
    input_db.create();

    // Set up output buffer
    psr::DadaDB output_db(16, output_buffer_bytes, 4,  header_size_bytes);
    output_db.create();

    // We are just creating an default initialised char
    // array to represent the input DADA data
    using InputDataType = std::vector<char>;

    // Setup a writer to the input DADA buffer
    psr::MultiLog producer_log("producer");
    psr::AsyncDadaSink writer(
        input_db.key(), producer_log, 
        psr::GenericHeaderFormatter([nchans](InputDataType const& data){
            psr::DadaHeader header;
            header.set("SAMPLE_CLOCK", 856000000.0);
            header.set("SAMPLE_CLOCK_START", 0);
            header.set("SYNC_TIME", 0.0);
            header.set("NCHANS", nchans);
            header.set("FREQUENCY", 1000.000);
            header.set("BANDWIDTH", 1000.000);
            header.set("TELESCOPE", "Effelsberg");
            header.set("SOURCE_NAME", "TestSource");
            header.set("SOURCE_RA", "00:00:00.000");
            header.set("SOURCE_DEC", "+00:00:00.000");
            header.set("BEAM_ID", "Primary");
            return header.to_vector();
        }), 
        psr::GenericDataFormatter()
    );

    // Here we are going to allow the writer to continously 
    // produce for the lifetime of the graph run below.
    bool input_stop = false;
    std::thread writer_thread(
        [&](){
            auto data_ptr = std::make_shared<InputDataType>(input_buffer_bytes, 0);
            while (!input_stop) {
                writer(data_ptr);
            }
        }
    );
    
    // Execute the graph. The graph will read from the input buffer and write to the 
    // output buffer. As the writer is running continously there should be no blocking
    // on the input. The output buffer may fill up, in which case the graph will block
    // on pushing to the output DADA buffer. This is the expected behaviour. The graph 
    // will only complete and exit when the DADA reader attaches and flushes the output
    // buffer. 
    FFTDetAccPipeline<fft_size, naccumulate> pipeline(input_db.key(), output_db.key());
    psr::Graph& graph = pipeline.graph();
    graph.run_async();
    // Run the graph for 1 second
    std::this_thread::sleep_for(1s);
    // Stop the graph from processing. Here it is assumed that the graph is not blocked
    // on the input (see above) but may or may not be blocked on pushing output
    graph.stop();

    // Tell the writer thread to stop producing
    input_stop = true;

    // Set up a reader to flush the output buffer
    psr::MultiLog consumer_log("consumer");
    psr::AsyncDadaSource source(output_db.key(), consumer_log, TestParser<float>());
    // Here we make a specific number of reads. We could also read until the buffer
    // is empty but that would result in the reader blocking last + 1 buffer.
    const std::size_t nreads = 10;
    for (std::size_t ii = 0; ii < nreads; ++ii) {
        auto result_tuple = source();
        auto const& result_ptr = std::get<0>(result_tuple);
        auto const& result = *result_ptr;
        auto const& span = result.data;
        auto const& header = result.header;
        std::cout << std::string(header.begin(), header.end()) << "\n";
    }

    // The ouput buffer for the graph is now flushed sufficiently for 
    // blocking writes from the graph to pass allowing the graph to 
    // finish gracefully.
    graph.await();

    // The stopped writer can now be safely joined.
    writer_thread.join();
}

} // namespace test
} // namespace edd_transient