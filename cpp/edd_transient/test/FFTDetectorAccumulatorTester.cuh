#pragma once
#include "edd_transient/FFTDetectorAccumulator.cuh"
#include <psrdada_cpp/dadaflow/DescribedData.hpp>
#include <psrdada_cpp/dadaflow/ResourcePool.hpp>
#include <psrdada_cpp/dadaflow/Dimensions.hpp>

#include <thrust/fill.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace edd_transient {
namespace test {

template <typename InputType_, typename OutputType_>
struct FFTDetectorAccumulatorTesterTraits
{
    using InputType = InputType_;
    using OutputType = OutputType_;
};

template <typename Traits>
class FFTDetectorAccumulatorTester : public ::testing::Test
{
public:
    using InputType = typename Traits::InputType;
    using OutputType = typename Traits::OutputType; 

    FFTDetectorAccumulatorTester(){}

    ~FFTDetectorAccumulatorTester(){}

    void SetUp() override {};

    void TearDown() override {};

    void populate_input(InputType& input, std::size_t nheaps, std::size_t nchans);
    void evaluate_output(InputType& input, OutputType const& output, std::size_t nchans, std::size_t naccumulate);
};

} // namespace test
} // namespace psrdada_cpp