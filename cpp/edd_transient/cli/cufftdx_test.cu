#include <cufftdx.hpp>

using namespace cufftdx;

template <typename FFT>
__global__ void block_fft_kernel(typename FFT::value_type* data,
                                 typename FFT::workspace_type workspace)
{
    using complex_type = typename FFT::value_type;

    // Registers
    complex_type thread_data[FFT::storage_size];

    // Local batch id of this FFT in CUDA block, in range [0;
    // FFT::ffts_per_block)
    const unsigned int local_fft_id = threadIdx.y;
    // Global batch id of this FFT in CUDA grid is equal to number of batches
    // per CUDA block (ffts_per_block) times CUDA block id, plus local batch id.
    const unsigned int global_fft_id =
        (blockIdx.x * FFT::ffts_per_block) + local_fft_id;

    // Load data from global memory to registers
    const unsigned int offset = cufftdx::size_of<FFT>::value * global_fft_id;
    const unsigned int stride = FFT::stride;
    unsigned int index        = offset + threadIdx.x;
    for(unsigned int i = 0; i < FFT::elements_per_thread; i++) {
        // Make sure not to go out-of-bounds
        if((i * stride + threadIdx.x) < cufftdx::size_of<FFT>::value) {
            thread_data[i] = data[index];
            index += stride;
        }
    }

    // FFT::shared_memory_size bytes of shared memory
    extern __shared__ __align__(alignof(float4)) complex_type shared_mem[];

    // Execute FFT
    printf("EXECUTING FFT\n");
    FFT().execute(thread_data, shared_mem, workspace);

    // Save results
    index = offset + threadIdx.x;
    for(unsigned int i = 0; i < FFT::elements_per_thread; i++) {
        if((i * stride + threadIdx.x) < cufftdx::size_of<FFT>::value) {
            printf("Result %d: %f\n", i, thread_data[i].x);
            data[index] = thread_data[i];
            index += stride;
        }
    }
}

void introduction_example(cudaStream_t stream = 0)
{
    // Base of the FFT description
    using FFT_base =
        decltype(Size<16>() + Precision<float>() + Type<fft_type::c2c>() +
                 Direction<fft_direction::forward>()
                 /* Notice lack of ElementsPerThread and FFTsPerBlock operators
                  */
                 + SM<890>() + Block());
    // FFT description with suggested FFTs per CUDA block for the default
    // (optimal) elements per thread
    using FFT          = decltype(FFT_base() + FFTsPerBlock<512>());
    using complex_type = typename FFT::value_type;

    printf("Block dim: %d, %d, %d\n",
           FFT::block_dim.x,
           FFT::block_dim.y,
           FFT::block_dim.z);
    printf("Stride: %d\n", FFT::stride);

    // Allocate managed memory for input/output
    complex_type* data;
    auto size       = FFT::ffts_per_block * cufftdx::size_of<FFT>::value;
    auto size_bytes = size * sizeof(complex_type);
    cudaMallocManaged(&data, size_bytes);
    // Generate data
    printf("---- Before ----\n");
    for(size_t i = 0; i < size; i++) {
        data[i] = complex_type{float(i), -float(i)};
        printf("%f + %fi\n", float(i), -float(i));
    }

    cudaError_t error_code = cudaSuccess;

    auto workspace = make_workspace<FFT>(error_code, stream);

    // Invokes kernel with FFT::block_dim threads in CUDA block
    block_fft_kernel<FFT>
        <<<1, FFT::block_dim, FFT::shared_memory_size, stream>>>(data,
                                                                 workspace);
    cudaDeviceSynchronize();

    printf("---- After ----\n");
    for(size_t i = 0; i < size; i++) {
        printf("%f + %fi\n", data[i].x, data[i].y);
    }

    cudaFree(data);
}

int main()
{
    introduction_example();
    return 0;
}