/**
 * This is the FFT detector and accumulator pipeline intended for use
 * with the EDD transient detection pipeline. Its input is data from
 * a polyphase filterbank in TFTP order and its output is sigproc
 * filterbank order data in floating point.
 */
#include "edd_transient/FFTDetAccPipeline.cuh"

#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/common.hpp>

#include <boost/program_options.hpp>

using namespace std::chrono;
using namespace boost::units;
using namespace boost::units::si;
using namespace psrdada_cpp;

namespace
{
const size_t ERROR_IN_COMMAND_LINE     = 1;
const size_t SUCCESS                   = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace

struct FFTDetAccArgs {
    key_t input_dada_key;
    key_t output_dada_key;
    unsigned tscrunch;
};

template <int nchans, int naccumulate>
void run(FFTDetAccArgs const& args)
{
    edd_transient::FFTDetAccPipeline<nchans, naccumulate> pipeline(
        args.input_dada_key,
        args.output_dada_key,
        args.tscrunch);
    Graph& graph = pipeline.graph();
    graph.show_dependencies();
    graph.show_topological_order();
    graph.make_dot_graph();
    GraphRunner::run(&graph);
    graph.show_statistics();
}

int main(int argc, char** argv)
{
    try {
        FFTDetAccArgs pipeline_args;
        std::string mode;
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()("help,h", "Print help messages")(
            "input-key,i",
            po::value<std::string>()->default_value("dada")->notifier(
                [&pipeline_args](std::string in) {
                    pipeline_args.input_dada_key = string_to_key(in);
                }),
            "The key for the input DADA buffer (hex key as string without "
            "leading 0x)")(
            "output-key,i",
            po::value<std::string>()->default_value("dadc")->notifier(
                [&pipeline_args](std::string in) {
                    pipeline_args.output_dada_key = string_to_key(in);
                }),
            "The key for the output DADA buffer (hex key as string without "
            "leading 0x)")(
            "mode,m",
            po::value<std::string>()->default_value("f16n128")->notifier(
                [&mode](std::string in) { mode = in; }),
            "The channelisation/accumulation mode to run in. Options are "
            "f8n256, f16n128, f32n64. "
            "(f number is fft size per coarse channel, n is number of channels "
            "to integrate)")(
            "nacc,n",
            po::value<unsigned>()->default_value(1)->notifier(
                [&pipeline_args](unsigned in) { pipeline_args.tscrunch = in; }),
            "Secondary accumulation to run on top of the accumulation "
            "specified by the mode argument, "
            "e.g. if mode=f16n128 and n=6 then the actual accumulation will be "
            "768")("log_level",
                   po::value<std::string>()->default_value("info")->notifier(
                       [](std::string level) { set_log_level(level); }),
                   "The logging level to use (debug, info, warning, error)");
        po::variables_map vm;
        try {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if(vm.count("help")) {
                std::cout << "fftdetacc -- perform channelisation detection "
                          << "and accumulation on Alveo PFB data" << std::endl
                          << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        } catch(po::error& e) {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        /**
         * All the application code goes here
         */

        if(mode == "f8n256") {
            run<8, 256>(pipeline_args);
        } else if(mode == "f16n128") {
            run<16, 128>(pipeline_args);
        } else if(mode == "f32n64") {
            run<32, 64>(pipeline_args);
        } else {
            throw std::runtime_error("Unsupported mode. Supported modes are: "
                                     "f8n256, f16n128, f32n64");
        }
        /**
         * End of application code
         */
    } catch(std::exception& e) {
        std::cerr << "Unhandled Exception reached the top of main: " << e.what()
                  << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;
}