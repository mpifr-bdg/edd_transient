#pragma once
#include "edd_transient/FFTDetectorAccumulator.cuh"

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/dadaflow/DadaHeader.hpp>
#include <psrdada_cpp/dadaflow/DescribedData.hpp>
#include <psrdada_cpp/dadaflow/Graph.hpp>
#include <psrdada_cpp/dadaflow/ResourcePool.hpp>
#include <psrdada_cpp/dadaflow/processors/AsyncDDCopy.cuh>
#include <psrdada_cpp/dadaflow/processors/AsyncDadaSink.hpp>
#include <psrdada_cpp/dadaflow/processors/AsyncDadaSource.hpp>
#include <psrdada_cpp/dadaflow/processors/MkrecvAlveoPFB64Parser.hpp>

#include <boost/json.hpp>
#include <sstream>
#include <iomanip>
#include <string>


namespace psr = psrdada_cpp;

namespace edd_transient
{

template <int nchans, int naccumulate>
class FFTDetAccPipeline
{
  public:
    using TFTPVoltageH = psr::Alveo64PFBData<std::span<char2>>;
    using TFTPVoltageD = psr::Alveo64PFBData<thrust::device_vector<char2>>;
    using TFPowerH     = FilterbankData<thrust::host_vector<float>>;
    using TFPowerD     = FilterbankData<thrust::device_vector<float>>;

    FFTDetAccPipeline(key_t input_key, key_t output_key, unsigned tscrunch = 1)
        : _h2d_allocator(3), _detacc_allocator(3), _d2h_allocator(3),
          _reader_log("FFTDetAccReader"), _writer_log("FFTDetAccWriter")
    {
        // --- Nodes ---
        auto& dada_source = _graph.add_node(
            psr::AsyncDadaSource(
                input_key,
                _reader_log,
                psr::MkrecvAlveoPFB64Parser<2048>([](TFTPVoltageH::MetadataType& metadata,
                                                psr::DadaHeader const& header) {
                    metadata["telescope"] = header.get<std::string>("TELESCOPE");
                    metadata["source_name"] =
                        header.get<std::string>("SOURCE_NAME");
                    metadata["ra"]   = header.get<std::string>("SOURCE_RA");
                    metadata["dec"]  = header.get<std::string>("SOURCE_DEC");
                    metadata["beam"] = header.get<std::string>("BEAM_ID");
                })),
            "DADA reader");

        using H2DCopyType =
            psr::AsyncDDCopy<TFTPVoltageH, TFTPVoltageD, psr::ResourcePool<TFTPVoltageD>>;
        auto& h2d_copy = _graph.add_node(H2DCopyType(_h2d_allocator),
                        "H2D copy");

        using FFTDetAccType = FFTDetectorAccumulator<
            nchans,
            naccumulate,
            TFTPVoltageD,
            TFPowerD,
            psr::ResourcePool<TFPowerD>>;
        auto& fft_det_acc = _graph.add_node(FFTDetAccType(tscrunch, _detacc_allocator),
                        "FFTDetAcc block");

        using D2HCopyType =
            psr::AsyncDDCopy<TFPowerD, TFPowerH, psr::ResourcePool<TFPowerH>>;
        auto& d2h_copy = _graph.add_node(D2HCopyType(_d2h_allocator),
                        "D2H copy");
        
        // For a more complex generic type like this we need to rely on a combination of 
        // CTAD and invocation traits to be able to add this to the graph.
        psr::AsyncDadaSink async_sink{
            output_key,
            _writer_log,
            psr::GenericHeaderFormatter([](TFPowerH const& data)
                                        -> std::vector<char> {
                BOOST_LOG_TRIVIAL(debug)
                    << "Generating header from: " << data.describe();
                boost::json::object json_obj;
                auto const& metadata = data.metadata();
                json_obj["telescope"] =
                    psr::safe_metadata_get<std::string>(metadata, "telescope");
                json_obj["source_name"] =
                    psr::safe_metadata_get<std::string>(metadata, "source_name");
                json_obj["ra"]   = psr::safe_metadata_get<std::string>(metadata, "ra");
                json_obj["dec"]  = psr::safe_metadata_get<std::string>(metadata, "dec");
                json_obj["beam"] = psr::safe_metadata_get<std::string>(metadata, "beam");
                std::stringstream formatter;
                formatter << std::fixed << std::setprecision(20)
                            << data.timestamp().mjd();
                json_obj["tstart"] = formatter.str();
                json_obj["nifs"]   = 1;
                json_obj["nbits"]  = 8 * sizeof(typename TFPowerH::value_type);
                json_obj["nchans"] = data.nchannels();
                json_obj["tsamp"] =
                    static_cast<double>(data.timestep().count());
                // frequencies are in Hertz but need to be converted to MHz
                json_obj["fch1"] =
                    (data.channel_zero_freq() / (1e6 * si::hertz)).value();
                json_obj["foff"] =
                    (data.channel_bandwidth() / (1e6 * si::hertz)).value();
                std::string json_str = boost::json::serialize(json_obj);
                return {json_str.begin(), json_str.end()};
            }),
            psr::GenericDataFormatter()};
            
        auto& dada_sink = _graph.add_node<
            decltype(async_sink), // Type of the sink
            psr::invocation_traits<
                // Traits of the callable if called with the output of the d2h_copy
                decltype(async_sink), typename std::remove_reference_t<decltype(d2h_copy)>::OutputValueType<0> 
            >
        > (std::move(async_sink), "DADA writer");

        _graph.connect<0, 0>(dada_source, h2d_copy, "dada->h2d", 3);
        _graph.connect<0, 0>(h2d_copy, fft_det_acc, "h2d->fftdetacc", 3);
        _graph.connect<0, 0>(fft_det_acc, d2h_copy, "fftdetacc->d2h", 3);
        _graph.connect<0, 0>(d2h_copy, dada_sink, "d2h->dada", 3);
        _graph.prepare();
    }

    psr::Graph& graph() { return _graph; }

  private:
    psr::Graph _graph;
    psr::ResourcePool<TFTPVoltageD> _h2d_allocator;
    psr::ResourcePool<TFPowerD> _detacc_allocator;
    psr::ResourcePool<TFPowerH> _d2h_allocator;
    psr::MultiLog _reader_log;
    psr::MultiLog _writer_log;
};

} // namespace edd_transient