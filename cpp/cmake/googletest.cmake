
if(ENABLE_TESTING)
    enable_testing()
    find_package(GTest QUIET)
    if (GTest_FOUND)
        message(STATUS "Googletest found! Version: ${GTest_VERSION}")
    else()
        message(STATUS "Gtest not found. Downloading GTest from remote..")
        include(FetchContent)
        FetchContent_Declare(
            googletest
            GIT_REPOSITORY https://github.com/google/googletest.git
            GIT_TAG v1.14.0
            GIT_PROGRESS TRUE
            INSTALL_COMMAND ""
        )
        FetchContent_MakeAvailable(googletest)
        set(GTEST_INCLUDE_DIR ${googletest_SOURCE_DIR}/googletest/include ${googletest_SOURCE_DIR}/googlemock/include)
    endif()
    set(GTEST_LIBRARIES GTest::gmock_main GTest::gmock GTest::gtest_main GTest::gtest)
endif(ENABLE_TESTING)