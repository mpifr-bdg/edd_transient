find_package(CUDAToolkit REQUIRED)

set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
set(CUDA_PROPAGATE_HOST_FLAGS OFF)

set(CMAKE_CUDA_FLAGS "--std=c++${CMAKE_CXX_STANDARD} -Wno-deprecated-gpu-targets")
set(CMAKE_CUDA_FLAGS_DEBUG "-O0 -G -Xcompiler -Wextra --Werror all-warnings -Xcudafe -Xptxas --verbose")
set(CMAKE_CUDA_FLAGS_PROFILE "${CMAKE_CUDA_FLAGS_DEBUG} --generate-line-info")
set(CMAKE_CUDA_FLAGS_RELEASE "-O3 -use_fast_math -restrict")
set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} ${CMAKE_CUDA_FLAGS_${uppercase_CMAKE_BUILD_TYPE}}")