include(ExternalProject)

set(MATHDX_VERSION "24.08.0")
set(MATHDX_URL "https://developer.download.nvidia.com/compute/cuFFTDx/redist/cuFFTDx/nvidia-mathdx-${MATHDX_VERSION}.tar.gz")
set(MATHDX_INSTALL_DIR "${CMAKE_BINARY_DIR}/mathdx")

ExternalProject_Add(
    mathdx
    URL ${MATHDX_URL}
    DOWNLOAD_DIR ${CMAKE_BINARY_DIR}/downloads
    DOWNLOAD_EXTRACT_TIMESTAMP ${MATHDX_VERSION}
    PREFIX ${CMAKE_BINARY_DIR}/mathdx
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND 
        ${CMAKE_COMMAND} -E make_directory ${MATHDX_INSTALL_DIR} &&
        tar --strip-components=4 -xzf ${CMAKE_BINARY_DIR}/downloads/nvidia-mathdx-${MATHDX_VERSION}.tar.gz -C ${MATHDX_INSTALL_DIR}
)
# set(mathdx_cufftdx_DISABLE_CUTLASS TRUE)
set(MATHDX_INCLUDE_DIR "${MATHDX_INSTALL_DIR}/include")
set(MATHDX_LIBRARY_DIR "${MATHDX_INSTALL_DIR}/lib")
add_library(mathdx::cufftdx INTERFACE IMPORTED)
set_target_properties(mathdx::cufftdx PROPERTIES
    INTERFACE_COMPILE_DEFINITIONS "CUFFTDX_DISABLE_CUTLASS_DEPENDENCY"
)
add_dependencies(mathdx::cufftdx mathdx)
include_directories(${MATHDX_INCLUDE_DIR})
