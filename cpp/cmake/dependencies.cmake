include(cmake/compiler_settings.cmake)
include(cmake/cuda.cmake)
include(cmake/googletest.cmake)
include(cmake/mathdx.cmake)
find_package(Boost COMPONENTS log program_options system json REQUIRED)
find_package(psrdada_cpp REQUIRED)

set(DEPENDENCY_LIBRARIES
    ${PSRDADA_CPP_LIBRARIES}
    ${CUDA_LIBRARIES}
    ${Boost_LIBRARIES}
    mathdx::cufftdx)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")

