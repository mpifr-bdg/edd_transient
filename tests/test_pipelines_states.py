"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description: State model testing of all pipelines
"""
import unittest

HOST = "127.0.0.1"
PORT = 1337

from mpikat.utils.testing import state_change_patch, Test_StateModel, Test_StateModelOnRequests
# Patch the state_change decorator / Must be called before importing pipeline implementations
unittest.mock.patch("mpikat.core.edd_pipeline_aio.state_change", wraps=state_change_patch).start()


from transient.pipelines import TransientSearch

class Test_TransientSearch_StateModel(Test_StateModel):
    def setUp(self) -> None:
        super().setUpBase(TransientSearch(HOST, PORT), False)

class Test_TransientSearch_StateModelOnRequests(Test_StateModelOnRequests):
    async def asynSetUp(self) -> None:
        await super().setUpBase(TransientSearch)

if __name__ == "__main__":
    unittest.main()
