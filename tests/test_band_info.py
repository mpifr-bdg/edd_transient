"""
Tests the band info class    
"""
import unittest
from aiokatcp.connection import FailReply
from transient.band_info import BandInfo

class TestBandInfo(unittest.TestCase):
    
    def setUp(self):
        # Sample configuration similar to your input_data_streams
        self.input_data_streams = [
            {
                "format": "AlveoPFB:1",
                "ip": "239.0.255.0+7",
                "port": "7148",
                "bit_depth": 8,
                "source": "alveo_pfb_controller:band0",
                "bandwidth": int(256),
                "central_freq": 1e3
            },
            {
                "format": "AlveoPFB:1",
                "ip": "239.0.255.1+7",
                "port": "7149",
                "bit_depth": 8,
                "source": "alveo_pfb_controller:band1",
                "bandwidth": int(256),
                "central_freq": 1e3 + (int(256))
            }
        ]
        # Instantiate the BandInfo class
        self.band_info = BandInfo(self.input_data_streams)
        
    def test_extract_band_info(self):
        # Verify extracted band numbers, central frequencies, and bandwidths
        self.assertEqual(self.band_info.band_numbers, [0, 1])
        self.assertEqual(self.band_info.band_central_freqs, [1e3,  1e3 + (int(256))])
        self.assertEqual(self.band_info.band_bandwidths, [int(256)] * 2)

    def test_calculate_band_edges(self):
        # Verify calculated start and end frequencies for each band
        self.band_info._calculate_band_edges()
        expected_starts = [1e3 - int(256) / 2, 1e3 + int(256) - int(256) / 2]
        expected_ends = [1e3 + int(256) / 2, 1e3 + int(256) + int(256) / 2]
        self.assertEqual(self.band_info.band_starts, expected_starts)
        self.assertEqual(self.band_info.band_ends, expected_ends)
    
    def test_check_band_continuity(self):
        # This should pass as bands are continuous
        try:
            self.band_info._check_band_continuity()
        except FailReply:
            self.fail("Band continuity check failed unexpectedly for continuous bands.")
        
        # Modify input_data_streams to make bands non-continuous and re-check
        self.band_info.band_ends[0] += 2  # Break continuity
        with self.assertRaises(FailReply):
            self.band_info._check_band_continuity()

    def test_calculate_nfchans_coarse(self):
        # Verify the number of coarse frequency channels (assuming 8 per band)
        self.band_info._calculate_nfchans_coarse()
        self.assertEqual(self.band_info.get_nfchans_coarse(), 16)

    def test_calculate_band_central_freq(self):
        # Verify calculation of overall central frequency
        self.band_info._calculate_band_central_freq()
        expected_central_freq = sum(self.band_info.band_central_freqs) / len(self.band_info.band_central_freqs)
        self.assertEqual(self.band_info.get_band_central_freq(), expected_central_freq)

    def test_calculate_total_bandwidth(self):
        # Verify calculation of total bandwidth
        self.band_info._calculate_bw()
        expected_bw_tot = sum(self.band_info.band_bandwidths)
        self.assertEqual(self.band_info.get_bw(), expected_bw_tot)


if __name__ == '__main__':
    unittest.main()
