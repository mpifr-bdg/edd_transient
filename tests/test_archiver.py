import unittest
import tempfile
import os
import time
import tarfile
import gzip
from transient.archiver import Archiver

class TestArchiverIntegration(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.TemporaryDirectory()
        self.directory = self.test_dir.name
        self.prefix = 'CAND'
        self.archiver = Archiver(self.directory, self.prefix, interval=1, stability_threshold=1)

    def tearDown(self):
        # Cleanup the temporary directory
        self.test_dir.cleanup()

    def test_archiver_compresses_and_archives_stable_files(self):
        # Start the archiver
        self.archiver.start()

        # Create files older than the stability threshold
        filenames = [f'{self.prefix}_oldfile{i}.cands' for i in range(3)]
        filepaths = [os.path.join(self.directory, filename) for filename in filenames]
        current_time = time.time()
        for filepath in filepaths:
            with open(filepath, 'w') as f:
                f.write('Old test data')
            # Set the modification time to be older than stability_threshold
            os.utime(filepath, (current_time - 5, current_time - 5))

        # Wait to allow archiving to occur
        time.sleep(2)

        # Stop the archiver (waits for the archiving thread to finish and closes the tarball)
        self.archiver.stop()

        # Wait briefly to ensure archiver has finalized
        time.sleep(0.5)

        # Now it's safe to access the tarball
        tarball_path = self.archiver.tarball_path
        print(f"Tarball path in test: {tarball_path}")

        # Verify that the tarball exists
        self.assertTrue(os.path.exists(tarball_path), f"Tarball does not exist at {tarball_path}")

        # Verify that the tarball contains the compressed files
        with tarfile.open(tarball_path, 'r') as tar:
            tar_files = tar.getnames()
            expected_files = [os.path.basename(f) + '.gz' for f in filepaths]
            self.assertEqual(sorted(tar_files), sorted(expected_files))

        # Verify that the original and compressed files have been removed
        for filepath in filepaths:
            self.assertFalse(os.path.exists(filepath), f"File was not removed: {filepath}")
            self.assertFalse(os.path.exists(filepath + '.gz'), f"Compressed file was not removed: {filepath + '.gz'}")

        # **Extract the tarball into a separate temporary directory**
        with tempfile.TemporaryDirectory() as extract_dir:
            with tarfile.open(tarball_path, 'r') as tar:
                tar.extractall(path=extract_dir)
            
            # Verify the contents of the extracted .gz files
            for filepath in filepaths:
                compressed_file = os.path.join(extract_dir, os.path.basename(filepath) + '.gz')
                self.assertTrue(os.path.exists(compressed_file), f"Extracted compressed file does not exist: {compressed_file}")
                with gzip.open(compressed_file, 'rt') as f:
                    content = f.read()
                    self.assertEqual(content, 'Old test data')
                # Clean up extracted .gz files
                os.remove(compressed_file)


    def test_archiver_ignores_recent_files(self):
        # Start the archiver
        self.archiver.start()

        # Create files newer than the stability threshold
        filenames = [f'{self.prefix}_newfile{i}.cands' for i in range(3)]
        filepaths = [os.path.join(self.directory, filename) for filename in filenames]
        current_time = time.time()
        for filepath in filepaths:
            with open(filepath, 'w') as f:
                f.write('New test data')
            # Set the modification time to current time
            os.utime(filepath, (current_time, current_time))

        # Wait less than the stability threshold to ensure files remain recent
        time.sleep(0.5)

        # Stop the archiver
        self.archiver.stop()

        # Verify that the files have been compressed and removed
        for filepath in filepaths:
            self.assertFalse(os.path.exists(filepath), f"File was not removed: {filepath}")
            self.assertFalse(os.path.exists(filepath + '.gz'), f"Compressed file was not removed: {filepath + '.gz'}")

        # Verify that the compressed files are in the tarball
        tarball_path = self.archiver.tarball_path
        self.assertTrue(os.path.exists(tarball_path), f"Tarball does not exist at {tarball_path}")

        with tarfile.open(tarball_path, 'r') as tar:
            tar_files = tar.getnames()
            expected_files = [os.path.basename(f) + '.gz' for f in filepaths]
            self.assertEqual(sorted(tar_files), sorted(expected_files))

    def test_archiver_final_compression_pass(self):
        # Create files newer than the stability threshold
        filenames = [f'{self.prefix}_newfile{i}.cands' for i in range(3)]
        filepaths = [os.path.join(self.directory, filename) for filename in filenames]
        for filepath in filepaths:
            with open(filepath, 'w') as f:
                f.write('Final pass test data')

        # Start and immediately stop the archiver to trigger final pass
        self.archiver.start()
        self.archiver.stop()

        # Verify that the tarball exists
        directory_name = os.path.basename(os.path.normpath(self.directory))
        tarball_path = os.path.join(self.directory, f"{directory_name}.tar")
        self.assertTrue(os.path.exists(tarball_path))

        # Verify that the tarball contains the compressed files
        with tarfile.open(tarball_path, 'r') as tar:
            tar_files = tar.getnames()
            expected_files = [os.path.basename(f) + '.gz' for f in filepaths]
            self.assertEqual(sorted(tar_files), sorted(expected_files))

        # Verify that the original and compressed files have been removed
        for filepath in filepaths:
            self.assertFalse(os.path.exists(filepath))
            self.assertFalse(os.path.exists(filepath + '.gz'))

    def test_archiver_handles_no_files(self):
        # Start the archiver
        self.archiver.start()
        # Wait briefly
        time.sleep(1)
        # Stop the archiver
        self.archiver.stop()
        # Verify that the tarball exists but is empty
        directory_name = os.path.basename(os.path.normpath(self.directory))
        tarball_path = os.path.join(self.directory, f"{directory_name}.tar")
        self.assertTrue(os.path.exists(tarball_path))
        with tarfile.open(tarball_path, 'r') as tar:
            tar_files = tar.getnames()
            self.assertEqual(len(tar_files), 0)

    def test_archiver_start_stop(self):
    # Start the archiver
        self.archiver.start()

        # Create a test file
        filename = f'{self.prefix}_testfile.cands'
        filepath = os.path.join(self.directory, filename)
        with open(filepath, 'w') as f:
            f.write('Test data')

        # Wait longer than stability_threshold + interval to allow archiving
        time.sleep(5)

        # Stop the archiver (ensures the tarball is finalized)
        self.archiver.stop()

        # Now verify the tarball
        tarball_path = self.archiver.tarball_path
        print(f"Tarball path in test: {tarball_path}")

        # Verify that the tarball exists and is not empty
        self.assertTrue(os.path.exists(tarball_path), f"Tarball does not exist at {tarball_path}")
        self.assertTrue(os.path.getsize(tarball_path) > 0, "Tarball is empty.")

        # Open the tarball
        with tarfile.open(tarball_path, 'r') as tar:
            tar_files = tar.getnames()
            expected_file = os.path.basename(filepath) + '.gz'
            self.assertIn(expected_file, tar_files)

        # Verify that the original and compressed files have been removed
        self.assertFalse(os.path.exists(filepath))
        self.assertFalse(os.path.exists(filepath + '.gz'))

if __name__ == '__main__':
    unittest.main()
