"""
Class to extract band info
"""
from typing import List
from aiokatcp.connection import FailReply
import mpikat.core.logger as logging

_LOG = logging.getLogger("mpikat.transient.band_info")

class BandInfo:
    def __init__(self, input_data_streams: List[dict]):
        """
        Initialize the BandInfo class with input data streams.

        Args:
            input_data_streams (List[dict]): List of dictionaries containing band information.
        """
        self.input_data_streams = input_data_streams
        self.band_numbers = []
        self.band_central_freqs = []
        self.band_bandwidths = []
        self.band_starts = []
        self.band_ends = []
        self.nfchans_coarse = 0
        self.band_central_freq = 0.0
        self.bw_tot = 0.0

        self._extract_band_info()
        self._calculate_band_edges()
        self._check_band_continuity()
        self._calculate_nfchans_coarse()
        self._calculate_band_central_freq()

    def _extract_band_info(self):
        """
        Extract band numbers, central frequencies, and bandwidths from input data streams.
        """
        self.band_numbers = [int(stream['source'].split('band')[-1])
                             for stream in self.input_data_streams]
        self.band_central_freqs = [stream['central_freq'] for stream in self.input_data_streams]
        self.band_bandwidths = [stream['bandwidth'] for stream in self.input_data_streams]

    def _calculate_band_edges(self):
        """
        Calculate the start and end frequencies of each band.
        """
        self.band_starts = [
            freq - bw / 2 for freq, bw in zip(self.band_central_freqs, self.band_bandwidths)
        ]
        self.band_ends = [
            freq + bw / 2 for freq, bw in zip(self.band_central_freqs, self.band_bandwidths)
        ]

    def _check_band_continuity(self):
        """
        Check if the bands are continuous.

        Raises:
            ValueError: If the bands are not continuous.
        """
        # Sort the bands based on their start frequencies
        bands = sorted(zip(self.band_starts, self.band_ends), key=lambda x: x[0])

        # Check if each band's end frequency matches the next band's start frequency
        for i in range(len(bands) - 1):
            end_freq = bands[i][1]
            start_freq_next = bands[i + 1][0]
            if abs(end_freq - start_freq_next) >= 1:  # Tolerance of 1 MHz
                _LOG.error("Frequency bands are not continuous between %.2f MHz and %.2f MHz", end_freq, start_freq_next)
                raise FailReply("Frequency bands are not continous")
        _LOG.info("Bands are continuous.")

    def _calculate_nfchans_coarse(self):
        """
        Calculate the number of coarse frequency channels.
        """
        self.nfchans_coarse = len(self.band_numbers) * 8  # Assuming 8 coarse channels per band

    def _calculate_band_central_freq(self):
        """
        Calculate the overall band central frequency.
        """
        self.band_central_freq = sum(self.band_central_freqs) / len(self.band_central_freqs)
        
    def _calculate_bw(self):
        """
        Calculate the number of coarse frequency channels.
        """
        self.bw_tot = sum(self.band_bandwidths) # Assuming 8 coarse channels per band

    def get_nfchans_coarse(self) -> int:
        """
        Get the number of coarse frequency channels.

        Returns:
            int: Number of coarse frequency channels.
        """
        return self.nfchans_coarse

    def get_band_central_freq(self) -> float:
        """
        Get the overall band central frequency.

        Returns:
            float: Band central frequency.
        """
        return self.band_central_freq
    
    def get_bw(self) -> float:
        """
        Returns BW
        """
        return self.bw_tot
