"""
EDD_transient: A Real-Time Search Plugin for Fast Transients

This plugin consists of four main components:
1. **Input Voltage Stream**: Provided by Alveo PFB - Mkrecv.
2. **Voltage to Intensity Converter**: Powered by FFT_DETACC.
3. **Dedispersion Tool**: Uses TransientX-based dedispersion (`dbdedisp`).
4. **Transient Search and Clustering Tool**: Utilizes `dbsearch` for transient search and clustering.

Configuration Settings:
-----------------------

- **input_data_streams** (dict): Specifies multiple data streams, where each stream represents a band containing 8 channels.

- **mode** (str): Defines the channelization and accumulation mode. Available options:
    - `f8n256`: FFT size of 8 per coarse channel with 256 integrations.
    - `f16n128`: FFT size of 16 per coarse channel with 128 integrations.
    - `f32n64`: FFT size of 32 per coarse channel with 64 integrations.

- **nacc** (int): Secondary accumulation applied on top of the `mode` setting.
    - Example: If `mode=f16n128` and `nacc=6`, the effective accumulation becomes 768.

- **dedisp** (dict): Configures TransientX dedispersion with the following key parameters:
    - `dms`: Starting value for dispersion measure (DM).
    - `ddm`: DM trial interval.
    - `ndm`: Number of DM trials to execute.
    https://github.com/ypmen/TransientX/blob/dada/examples/dbdedispdb_config.json

- **dbsearch** (dict): Configures TransientX single pulse search and clustering with these key parameters:
    - `minw`: Minimum pulse width.
    - `maxw`: Maximum pulse width.
    https://github.com/ypmen/TransientX/blob/dada/examples/dbsearch_config.json

- **plot_interval_t** (float, seconds): Interval for real-time DM-T candidate plotting.
"""

import os
import asyncio
import datetime
import json
import tempfile
import re
import time
import math

from aiokatcp.sensor import Sensor
from aiokatcp.connection import FailReply
from mpikat.core.datastore import EDDDataStore
from mpikat.utils import dada_tools as dada
from mpikat.utils import mk_tools as mk
from mpikat.utils.process_tools import ManagedProcess
from mpikat.utils.sensor_watchdog import SensorWatchdog, conditional_update
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.utils.core_manager import CoreManager
from mpikat.utils import data_streams
from mpikat.utils.output import create_datebased_subdirs
from mpikat.utils import numa
import mpikat.core.logger as logging

_LOG = logging.getLogger("mpikat.transient.pipelines.transient_search")

from transient.archiver import Archiver
from transient.band_info import BandInfo

_DEFAULT_CONFIG = {
    "id": "TransientSearch",
    "type": "TransientSearch",
    "input_type": "network",
    "search_type": "dbsearch",
    "dedispersion": "dedisp",
    "output_directory": "/mnt/",
    "plot_interval_t": 10,                  #in sec
    "npols": 2,                             # no.of polarizations - hard coded
    "samples_per_heap": 2048,               # product of mode (T) - hard coded
    "nacc": 4,                              # fft_detacc_default nacc
    "mode": 'f16n128',                      # fft_detacc_default f**n**
    "input_data_streams":
    [
        {
            "format": "AlveoPFB:1",         # Format has version seperated via colon
            "ip": "239.0.255.0+7",
            "port": "7148",
            "bit_depth": 8,
            "source": "alveo_pfb_controller:band0",
                "sample_rate": int(1.024e9),
            "bandwidth": int(1.024e9) / 64,
            "center_freq": 1e3
        }
    ],
    "dedisp": {"preprocesslite": {
        "td": 1,
        "fd": 1,
        "zapthre": 3.0,
        "filltype": "mean"
    },
        "downsample": {
            "td": 1,
            "fd": 1
    },
        "baseline": {
            "width": 0.0
    },
        "rfi": {
            "filltype": "mean",
            "thremask": 7.0,
            "threKadaneF": 10.0,
            "threKadaneT": 7.0,
            "widthlimit": 0.05,
            "bandlimitKT": 10.0            # "zaplist": [
            #     [800.0, 900.0],
            #     [1000.0, 1100.0]]
            ,
            "rfilist": [
                ["kadaneF", "4", "8"] ,
                 ["zdot"]
            ]
    },
        "subdedispersion": {
            "rootname": "fake",
            "dms": 0.0,
            "ddm": 0.1,
            "ndm": 4096,
            "overlap": 0.0
    }
    },
    "dbsearch": {
        "boxcar": {
            "minw": 1e-4,
            "maxw": 0.1,
            "snrloss": 0.1,
            "iqr": False
        },

        "clustering": {
            "thre": 6.0,
            "radius": 1.0,
            "neighbors": 2,
            "maxncand": 100,
            "minpts": 5,
            "drop": False
        },

        "candplot":{
            "plan_id": 0,
            "rootname": "CAND",            # Change?
            "saveimage": False
        }
    },
    "archiving":{                          #if not to be exposed to users, can be hard coded.
        "prefix": "CAND",
        "interval": 10,                     # in seconds
        "stability_threshold": 5            # in seconds
    },
    "det_acc_config": {
        "threshold": 5.0,
        "accumulation_time": 10.0,
        "buffer_size": 2048
    },
    "data_store": {
        "ip": "edgar-inf00",
        "port": 6379
    },
}


def recursive_update(d, u):
    """
    For recursive update of config file
    """
    if not u:
        return d
    for k, v in u.items():
        if isinstance(v, dict) and k in d and isinstance(d[k], dict):
            d[k] = recursive_update(d[k], v)
        else:
            d[k] = v
    return d


def closest_power_of_two(n):
    """
    Find the closest power of 2 near n.
    """
    if n <= 0:
        return 1
    log2_n = math.log2(n)
    nearest_exponent = round(log2_n)
    return 2 ** nearest_exponent

def get_numa_nodes():
    """
    Set NUMA nodes
    """
    numa_nodes = []
    # Get NUMA node and its charachteristics
    for node_id, desc in numa.getInfo().items():
        # remove numa nodes with missing capabilities
        if len(desc['gpus']) < 1:
            _LOG.debug(
                "Not enough gpus on numa node %i - removing from pool.",
                node_id)
        elif len(desc['net_devices']) < 1:
            _LOG.debug(
                "Not enough nics on numa node %i - removing from pool.",
                node_id)
        else:
            numa_nodes.append(node_id)

    _LOG.info(
        "NUMA nodes %i remain in pool after constraints.",
        len(numa_nodes))
    if len(numa_nodes) == 0:
        _LOG.error("No NUMA node with required capablities available!")
        raise FailReply("No NUMA node with required capablities available")
    return numa_nodes


class TransientSearch(EDDPipeline):
    def __init__(
            self,
            ip: str,
            port: int,
            loop: asyncio.AbstractEventLoop = None):
        """
        Description:
            Construct the TransientSearch pipeline.

        Args:
            - ip (str): The ip of the KATCP server
            - port (int): The port of the KATCP server
            - loop (asyncio.AbstractEventLoop, optinal): Defaults to None
        """
        # Call to the base class for constructing the pipeline
        super().__init__(ip, port, default_config=dict(_DEFAULT_CONFIG), loop=loop)
        _LOG.info("TransientSearch pipeline instantiated")
        self.fftacc_handler: ManagedProcess = None
        self.edd_data_store: EDDDataStore = None
        self._buffer_sensors = {}
        self._input_process: ManagedProcess = None
        self._dedisp_process: ManagedProcess = None
        self._dbsearch_process: ManagedProcess = None
        self.plotting_script: ManagedProcess = None
        self._core_manager: CoreManager = CoreManager()
        self._numa_node = None
        self.fftacc_input_buffer: dada.DadaBuffer = None
        self.fftacc_output_buffer: dada.DadaBuffer = None
        self.dedisp_output_buffer_f: dada.DadaBuffer = None
        self.dedisp_output_buffer_t: dada.DadaBuffer = None
        self._watchdog: SensorWatchdog = None
        self.mk_sensor = mk.MkrecvSensors("")
        self.output_path = None
        self._png_plot = None
        self.archiver: Archiver = None
        self.band_info: BandInfo = None

    def setup_sensors(self):
        """Setups KATCP sensors. You can define as many sensor as you like
        """
        # Call to the base class for setting up common sensors
        super().setup_sensors()
        # The aiokatcp.DeviceServer owns a set() of sensors (self.sensors)
        self.sensors.add(Sensor(str, "example-name",
                         description="I am an example sensor"))

        self._png_plot: Sensor = self.sensors.add(
            Sensor(str,
                   "bandpass",
                   description="band-pass data (base64 encoded)",
                   initial_status=Sensor.Status.UNKNOWN))

    @state_change(target="configured",
                  allowed=["idle"],
                  intermediate="configuring")
    async def configure(self, config: dict = None):
        """
        Configure the EDD VLBI pipeline and set up the necessary DADA buffers.
        """
        _LOG.info("Configuring Transient pipeline")

        config = config or {}
        self._config = recursive_update(self._config, config)

        _LOG.info("Configuration file updated %s", config)

        self._numa_node = get_numa_nodes()[0]
        self.edd_data_store = EDDDataStore(
            self._config["data_store"]["ip"],
            self._config["data_store"]["port"])

        try:
            self.band_info = BandInfo(self._config['input_data_streams'])
        except ValueError as e:
            _LOG.error("Band configuration error: %s", e)
            raise FailReply(str(e))
        
        nfchans_coarse = self.band_info.get_nfchans_coarse()

        try:
            # Latency param for input buffer -- 1GB input buffer

            # 2--accounts for complex vals-char2
            n_pre = int(1e9 /
                        (self._config["npols"] *
                         nfchans_coarse *
                         self._config["samples_per_heap"] *
                            self._config["nacc"] *
                            2))
            N = closest_power_of_two(n_pre)
            _LOG.info("Calculated N: %s", N)

            n_fftpoints_match = re.search(r'f(\d+)', self._config["mode"])
            if n_fftpoints_match:
                # Convert the matched string to an integer
                n_fftpoints = int(n_fftpoints_match.group(1))
            else:
                _LOG.error("Invalid mode format for detection accumulator")
                raise FailReply(
                    "Invalid mode format: {}".format(self._config["mode"]))

            # some-check for nacc
            fft_tscruch = self._config["samples_per_heap"] / n_fftpoints
            t_out_fdec = N * \
                self._config["samples_per_heap"] / \
                (fft_tscruch * self._config["nacc"])
            fftacc_input_size = int(
                N *
                self._config["npols"] *
                nfchans_coarse *
                self._config["samples_per_heap"] *
                self._config["nacc"] *
                2)
            # 4 for float to bytes
            fftacc_output_size = int(
                t_out_fdec * nfchans_coarse * n_fftpoints * 4)

        # Create DADA input and output buffer for fftacc-----------------------
            self.fftacc_input_buffer = dada.DadaBuffer(
                fftacc_input_size, node_id=self._numa_node, pin=True, lock=True)
            await self.fftacc_input_buffer.create()

            # , n_reader=2)#--fata capture from fft_det_ac needs this
            self.fftacc_output_buffer = dada.DadaBuffer(
                fftacc_output_size, node_id=self._numa_node, pin=True, lock=True)
            await self.fftacc_output_buffer.create()
        # -----------------------------------------------------------------------

        # Create DADA output buffer for dedispersion output--------------------
            self.dedisp_output_buffer_t = dada.DadaBuffer(1)
            self.dedisp_output_buffer_f = dada.DadaBuffer(1)
        # ------------------------------------------------------------------
            _LOG.debug("DADA buffers created and pipeline is configured.")

        # Sensor creation and monitor -----------------------------------------
            self.add_fftdetacc_input_sensors(self.fftacc_input_buffer.key)
            self.add_fftdetacc_output_sensors(self.fftacc_output_buffer.key)

            self.fftacc_input_buffer.get_monitor(
                self.buffer_status_handle).start()
            self.fftacc_output_buffer.get_monitor(
                self.buffer_status_handle).start()

            for sensor in self.mk_sensor.sensors:
                self.sensors.add(sensor)

            self.mass_inform("interface-changed")
            _LOG.info("Buffer sensors created.")

        except Exception as e:
            _LOG.error("Failed to create DADA buffers:%s", e)
            # Raise an exception to signal the pipeline could not be configured
            raise FailReply(
                "Pipeline configuration failed due to buffer creation error.") from e
        if self._config["input_type"] == "network":
            self._core_manager.add_task("input_process", 8+1, True)
            self._core_manager.add_task("input_process", 8 + 1, True)

    @state_change(target="set",
                  allowed=["ready"],
                  intermediate="measurement_preparing")
    async def measurement_prepare(self, config: dict = None):
        """
        Prepare the measurement, start FFT Dec and the dedispersion process.
        """
        _LOG.info("TransientPipeline received measurement-prepare")

        config = config or {}
        self._config = recursive_update(self._config, config)
        self.inputparam_sanity()

        # -------Output Directory  created-------------------------------------
        subdir_path = create_datebased_subdirs(
            self._config["output_directory"])

        band_central_freq = self.band_info.get_band_central_freq()

        obs_start_time_str = self.edd_data_store.getTelescopeDataItem(
            "start_time")
        obs_start_time = datetime.datetime.strptime(
            obs_start_time_str, "%Y-%m-%dT%H:%M:%SZ")
        obs_start_time_formatted = obs_start_time.strftime("%Y%m%dT%H%M%SZ")
        new_subdir_name = f"UTC{obs_start_time_formatted}FREQ{band_central_freq:.0f}"
        self.output_path = os.path.join(subdir_path, new_subdir_name)
        os.makedirs(self.output_path, exist_ok=True)
        _LOG.info("Output directory created at: %s", self.output_path)
        os.chdir(self.output_path)
        _LOG.info("Current working directory changed to:%s", os.getcwd())

        scan_meta = self._retrieve_telescope_metadata()
        if self.output_path:
            metadata_file_path = os.path.join(
                self.output_path, "scan_meta.json")
            try:
                with open(metadata_file_path, 'w', encoding="utf-8") as f:
                    json.dump(scan_meta, f, indent=4)
                _LOG.info("Metadata written to %s", metadata_file_path)
            except Exception as e:
                _LOG.warning("Failed to write metadata file: %s", e)
        else:
            _LOG.warning(
                "Output directory not set. Cannot write metadata file.")

        # -------------FFT dettac starts-------------------------------
        fftacc_cmd = f'fftdetacc --input-key {self.fftacc_input_buffer.key} --output-key {self.fftacc_output_buffer.key} --mode {self._config["mode"]} --nacc {self._config["nacc"]} '
        self.fftacc_handler = ManagedProcess(fftacc_cmd)

        _LOG.info("FFTacc initiated")

    # Create a unique json config file for each run--------dedisp_cmd and plot
        with tempfile.NamedTemporaryFile(delete=False, suffix='.json', mode='w') as temp_config_file:
            dedisp_config_file = temp_config_file.name
            dedisp_config = self._config.get("dedisp", {})
            json.dump(dedisp_config, temp_config_file)

        _LOG.info("Dedispersion configuration saved to: %s",
                  dedisp_config_file)
        self.start_dedisp_process(dedisp_config_file)

        # directory of cands file is current file
        plt_cmd = f'DMT_plotter --save_interval {self._config["plot_interval_t"]} --prefix {self._config["dbsearch"]["candplot"]["rootname"]}'
        
        self.plotting_script = ManagedProcess(
            plt_cmd, stdout_handler=self.script_stdout_parser)
        
        self.archiver = Archiver(
        directory=self.output_path,
        prefix=self._config["dbsearch"]["candplot"]["rootname"])
        self.archiver.start()
        _LOG.info("Archiving started")

    @state_change(target="measuring",
                  allowed=["set"],
                  intermediate="measurement_starting")
    async def measurement_start(self):
        """
        start measuring
        """

        self.start_input_process()
        _LOG.info("TransientPipeline received measurement-start")

        with tempfile.NamedTemporaryFile(delete=False, suffix='.json', mode='w') as temp_config_file:
            dbsearch_config_file = temp_config_file.name
            dbsearch_config = self._config.get("dbsearch", {})
            try:
                json.dump(dbsearch_config, temp_config_file)
            except Exception as e:
                _LOG.error("Failed to write DBSearch configuration:%s", e)

        _LOG.info("Dedispersion configuration saved to: %s",
                  dbsearch_config_file)

        self.start_dbsearch_process(dbsearch_config_file)

# -------Watch dog for input buffer write---------------------------------
        nic, params = numa.getFastestNic(self._numa_node)
        self._watchdog = SensorWatchdog(
            self._buffer_sensors["input_buffer_total_write"],
            10, self.watchdog_error)
        self._watchdog.start()
        _LOG.info("Receiving on %s [%s] @ %s Mbit/s",
                  nic, params['ip'], params['speed'])

    @state_change(target="ready",
                  allowed=["measuring",
                           "set"],
                  ignored=["ready"],
                  intermediate="measurement_stopping")
    async def measurement_stop(self):
        """
        stop measuring output
        """
        _LOG.info("TransientPipeline received measurement-stop")
        await self._cleanup_resources(reset_buffers=True)
        _LOG.info("Measurement stopped.")

    @state_change(target="ready",
                  allowed=["configured"],
                  intermediate="configured")
    async def capture_start(self):
        """start streaming output
        """
        # No operation -> is usually used in streaming and not measuring
        # pipelines
        _LOG.debug("Started capturing")

    @state_change(target="idle",
                  allowed=["ready"],
                  intermediate="capture_stopping")
    async def capture_stop(self):
        """stop streaming of data
        """
        # No operation -> is usually used in streaming and not measuring
        # pipelines
        _LOG.debug("Stopped capturing")

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """deconfigure the pipeline.
            Note: The pipeline should come back to the initial state -> all buffers, subprogram/tasks should be closed here
        """
        # Killing all the process pool
        _LOG.debug("Deconfigure processes ...")
        await self._cleanup_resources(reset_buffers=False)
        _LOG.info("Deconfigured.")

    async def _cleanup_resources(self, reset_buffers=False):
        """
        Internal method to clean up resources during measurement stop and deconfigure.

        Args:
            reset_buffers (bool): If True, reset buffers; if False, destroy buffers.
        """
        _LOG.debug("Cleaning up resources...")

        # Stop the watchdog
        if self._watchdog is not None:
            self._watchdog.stop_event.set()
            self._watchdog = None
            _LOG.debug("Watchdog stopped.")

        # Terminate input process
        if self._input_process is not None:
            if isinstance(self._input_process, ManagedProcess):
                self._input_process.terminate()
            elif hasattr(self._input_process, 'stop'):
                self._input_process.stop()
            else:
                _LOG.warning("_input_process process does not support termination")
            self._input_process = None
            _LOG.debug("Input process terminated.")

        # Terminate FFTACC handler
        if self.fftacc_handler is not None:
            self.fftacc_handler.terminate()
            self.fftacc_handler = None
            _LOG.debug("FFTACC handler terminated.")

        # Terminate dedispersion process
        if self._dedisp_process is not None:
            if isinstance(self._dedisp_process, ManagedProcess):
                self._dedisp_process.terminate()
            elif hasattr(self._dedisp_process, 'stop'):
                self._dedisp_process.stop()
            else:
                _LOG.warning("dedisp process does not support termination")
            self._dedisp_process = None
            _LOG.debug("Dedispersion process terminated.")

        # Terminate dbsearch process
        if self._dbsearch_process is not None:
            if isinstance(self._dbsearch_process, ManagedProcess):
                self._dbsearch_process.terminate()
            elif hasattr(self._dbsearch_process, 'stop'):
                self._dbsearch_process.stop()
            else:
                _LOG.warning("dbsearch process does not support termination")
            self._dbsearch_process = None
            _LOG.debug("dbsearch process terminated.")

        # Terminate plotting script
        if self.plotting_script is not None:
            if isinstance(self.plotting_script, ManagedProcess):
                self.plotting_script.terminate()
            else:
                _LOG.warning("plotting_script process does not support termination")
            self.plotting_script = None
            _LOG.debug("Plotting script terminated.")

        # Stop archiving and perform final compression
        if self.archiver:
            self.archiver.stop()
            self.archiver = None
            _LOG.info("Archiving stopped and final compression completed.")
        else:
            _LOG.warning("Archiver was not running.")

        if self.output_path:
            _LOG.info("Archiving completed. Output path: %s", self.output_path)
            self.output_path = None
        else:
            _LOG.warning("Output path is not set. Cannot confirm archiving completion.")

        # Handle buffers
        try:
            if reset_buffers:
                # Reset fftacc buffers at measurement stop
                _LOG.debug("Resetting fftacc buffers...")
                if self.fftacc_output_buffer is not None:
                    await self.fftacc_output_buffer.reset()
                    _LOG.debug("FFTACC output buffer reset.")
                if self.fftacc_input_buffer is not None:
                    await self.fftacc_input_buffer.reset()
                    _LOG.debug("FFTACC input buffer reset.")
                # Destroy dedispersion buffers
                _LOG.debug("Destroying dedispersion output buffers...")
                if self.dedisp_output_buffer_t is not None:
                    await self.dedisp_output_buffer_t.destroy()
                    self.dedisp_output_buffer_t = None
                    _LOG.debug("Dedispersion time buffer destroyed.")
                if self.dedisp_output_buffer_f is not None:
                    await self.dedisp_output_buffer_f.destroy()
                    self.dedisp_output_buffer_f = None
                    _LOG.debug("Dedispersion frequency buffer destroyed.")
            else:
                # Destroy all buffers at deconfigure
                _LOG.debug("Destroying all buffers...")
                if self.fftacc_input_buffer is not None:
                    await self.fftacc_input_buffer.destroy()
                    self.fftacc_input_buffer = None
                    _LOG.debug("FFTACC input buffer destroyed.")
                if self.fftacc_output_buffer is not None:
                    await self.fftacc_output_buffer.destroy()
                    self.fftacc_output_buffer = None
                    _LOG.debug("FFTACC output buffer destroyed.")
                if self.dedisp_output_buffer_t is not None:
                    await self.dedisp_output_buffer_t.destroy()
                    self.dedisp_output_buffer_t = None
                    _LOG.debug("Dedispersion time buffer destroyed.")
                if self.dedisp_output_buffer_f is not None:
                    await self.dedisp_output_buffer_f.destroy()
                    self.dedisp_output_buffer_f = None
                    _LOG.debug("Dedispersion frequency buffer destroyed.")
        except Exception as e:
            _LOG.error("Failed to reset or destroy buffers: %s", e)

        _LOG.debug("Resource cleanup completed.")

    def start_input_process(self):
        """Start the output process, which can be
            1 network = mkreceive
            2 disk = dada_dbdisk;
            3 junkdb = junkdb -- to check with out fftdetac

        Args:
            stream_id (str): The stream ID
            node (int): The node ID where to run the output handler
        """
        dada_header = self.create_header()
        if self._config['input_type'] == "disk":
            if not os.path.exists(self._config["input_file"]):
                _LOG.error("File %s does not exists",
                           self._config['input_file'])
                raise FailReply(
                    f"File {self._config['input_file']} does not exists")
            handler = dada.DiskDb(self.fftacc_input_buffer.key, self._config["input_file"], header=dada_header.write(False),
                                  n_reads=10000)
            handler = dada.DiskDb(
                self.fftacc_input_buffer.key,
                self._config["input_file"],
                header=dada_header.write(False),
                n_reads=10000)

        elif self._config['input_type'] == "network":
            band_numbers = [int(stream['source'].split('band')[-1])
                            for stream in self._config['input_data_streams']]
            fastest_nic, nic_params = numa.getFastestNic(self._numa_node)
            _LOG.info("Receiving data on NIC %s [ %s ] @ %s Mbit/s",
                      fastest_nic, nic_params['ip'], nic_params['speed'])

            handler = mk.Mkrecv(
                self._config["input_data_streams"],
                self.fftacc_input_buffer.key,
                physcpu=self._core_manager.get_coresstr("input_process"))
            handler.header.add(dada_header)
            handler.set_header("packet_size", 8400)
            handler.set_header("buffer_size", 128000000)
            handler.set_header("dada_nslots", 3)
            handler.set_header(
                "heap_nbytes",
                self._config["input_data_streams"][0]["samples_per_heap"])
            handler.set_header("ibv_if", nic_params["ip"])

            handler.set_item("timestep", "step", 262144)
            handler.set_item("timestep", "index", 1)
            # handler.set_item("timestep", "modulo", modulo)
            handler.set_item("channel_id", "list", range(
                min(band_numbers) * 8, (max(band_numbers) + 1) * 8))
            handler.set_item("channel_id", "index", 2)
            handler.stdout_handler(self.mk_sensor.stdout_handler)

        else:
            handler = dada.JunkDb(
                self.fftacc_input_buffer.key,
                duration=3600,
                header=dada_header.write(False))  # header file added

        self._input_process = handler.start()

    def start_dedisp_process(self, dedisp_conf):
        """Start the output process, which can be
            1 dedisp = dedispersion
            2 null = dada_dbnull
        """

        if self._config['dedispersion'] == "dedisp":
            dedispersion_cmd = f"dada_dbdedispdb -v --config {dedisp_conf} --key_input {self.fftacc_output_buffer.key} --key_output {self.dedisp_output_buffer_t.key} --key_output2 {self.dedisp_output_buffer_f.key}"
            _LOG.info("dispers command %s", dedispersion_cmd)
            handler = ManagedProcess(dedispersion_cmd)
            self._dedisp_process = handler

        else:
            # header file added
            handler = dada.DbNull(self.fftacc_output_buffer.key)
            self._dedisp_process = handler

    def start_dbsearch_process(self, dbsearch_conf):
        """Start the output process, which can be
            1 dbsearch = seach for transients
            2 null = dada_dbnull
        """
        if self._config['search_type'] == "dbsearch":
            dbsearch_cmd = f"dada_dbsearch -v --config {dbsearch_conf} --key_input {self.dedisp_output_buffer_t.key} --key_input2 {self.dedisp_output_buffer_f.key}"
            _LOG.info("dispers command %s", dbsearch_cmd)
            handler = ManagedProcess(dbsearch_cmd)
            self._dbsearch_process = handler
        else:
            handler = dada.DbDisk(
                self.dedisp_output_buffer_t.key, self._config['dedisp_dump'])
            _LOG.info("Writing output to %s", self._config['dedisp_dump'])
            self._dbsearch_process = handler.start()

    def _retrieve_telescope_metadata(self):

        data = {}
        data['source_name'] = self.edd_data_store.getTelescopeDataItem(
            "source-name")
        data["ra"] = self.edd_data_store.getTelescopeDataItem("ra")
        data["dec"] = self.edd_data_store.getTelescopeDataItem("dec")
        data['obs_id'] = self.edd_data_store.getTelescopeDataItem(
            "observation-id")
        data['receiver_name'] = self.edd_data_store.getTelescopeDataItem(
            "receiver")
        # os.join(self._config["output_directory"], scanid)
        _LOG.info(
            "Retrieved data from telescope:\n Source name: %s,\n RA = %s,  decl = %s, receiver = %s", {
                data['source_name']}, {
                data['ra']}, {
                data['dec']}, {
                    data['receiver_name']})

        # header_string = f"source_name={data['source_name']},ra={data['ra']},dec={data['dec']},obs_id={data['obs_id']},receiver_name={data['receiver_name']}"
        return data

    def create_header(self):
        """Creates dada header for junkDB"""

        dadaheader = dada.DadaHeader()
        dadaheader.add(dada.DadaBaseHeader())
        dadaheader.add(dada.SigProcHeader())

        data = {}
        data['source_name'] = self.edd_data_store.getTelescopeDataItem(
            "source-name")
        data["source_ra"] = self.edd_data_store.getTelescopeDataItem("ra")
        data["source_dec"] = self.edd_data_store.getTelescopeDataItem("dec")
        data['obs_id'] = self.edd_data_store.getTelescopeDataItem(
            "observation-id")
        data['receiver_name'] = self.edd_data_store.getTelescopeDataItem(
            "receiver")
        data['telescope'] = "Eff"
        data['bandwidth'] = self.band_info.get_bw()
        data['frequency'] = self.band_info.get_band_central_freq()

        # for key, value in data.items():
        #    dadaheader.set(key, value)
        dadaheader.update(data)
        # MK reciev related header will be retrieved later
        dadaheader.set('sample_clock_start', 0)
        # beware this calculation is using BW in MHz
        dadaheader.set('sample_clock', 2*self.band_info.get_bw())
        dadaheader.set('SYNC_TIME', time.time())
        dadaheader.set('NCHANS', self.band_info.get_nfchans_coarse())

        header_info = "\n".join(
            [f"{key}: {dadaheader.get(key)}" for key in data.keys()])
        _LOG.info("Header information:\n %s", header_info)

        return dadaheader

    def add_fftdetacc_input_sensors(self, key: str):
        """
        add sensors for i/o buffers for an output stream with given key
        """
        if key not in self._buffer_sensors:
            self._buffer_sensors[key] = {}
        self._buffer_sensors[key]["input_buffer_fill_level"] = Sensor(
            float, "input-buffer-fill-level",
            description="Fill level of fftdetacc input buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[key]["input_buffer_total_write"] = Sensor(
            float, "input-buffer-total-write",
            description="Total write to fftdetacc_input buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._buffer_sensors[key]["input_buffer_total_write"])
        self.sensors.add(self._buffer_sensors[key]["input_buffer_fill_level"])

    def add_fftdetacc_output_sensors(self, key: str):
        """
        add sensors for i/o buffers for an output stream with given key
        """
        if key not in self._buffer_sensors:
            self._buffer_sensors[key] = {}
        self._buffer_sensors[key]["output_buffer_fill_level"] = Sensor(
            float, "output-buffer-fill-level",
            description="Fill level of fftdetacc output buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[key]["output_buffer_total_write"] = Sensor(
            float, "output-buffer-total-write",
            description="Total write to fftdetacc buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[key]["output_buffer_total_read"] = Sensor(
            float, "output-buffer-total-read",
            description="Total read of fftdetacc_output buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(
            self._buffer_sensors[key]["output_buffer_total_write"])
        self.sensors.add(self._buffer_sensors[key]["output_buffer_total_read"])
        self.sensors.add(self._buffer_sensors[key]["output_buffer_fill_level"])

    def buffer_status_handle(self, status: dict):
        """
        Process a change in the buffer status
        """
        timestamp = time.time()
        if status["key"] not in self._buffer_sensors:
            _LOG.warning("buffer key not found to update")
            return
        sensor = self._buffer_sensors[status['key']]
        # Sensor update -- fftdetacc input
        if status['key'] == self.fftacc_input_buffer.key:
            conditional_update(
                sensor["input_buffer_total_write"],
                status['written'],
                timestamp=timestamp)
            sensor["input_buffer_fill_level"].set_value(
                status['fraction-full'], timestamp=timestamp)
        # Sensor update -- fftdetacc output
        if status['key'] == self.fftacc_output_buffer.key:
            conditional_update(
                sensor["output_buffer_total_write"],
                status['written'],
                timestamp=timestamp)
            conditional_update(
                sensor["output_buffer_total_read"],
                status['read'],
                timestamp=timestamp)
            sensor["output_buffer_fill_level"].set_value(
                status['fraction-full'], timestamp=timestamp)

    def inputparam_sanity(self):
        """
        Check if the config parameters are reasonable.
        """
        # Access the dedisp configuration
        dedisp_config = self._config.get('dedisp', {})
        subdedisp_config = dedisp_config.get('subdedispersion', {})

        # Validate 'ndm' for DM trials
        ndm = subdedisp_config.get('ndm', 0)
        if ndm > 10000:
            _LOG.error("Too many DM trials: ndm = %s", ndm)
            raise FailReply(f"Too many DM trials, ndm {ndm}")

        # Validate DM range based on 'ndm' and 'ddm'
        ddm = subdedisp_config.get('ddm', 0)
        dm_range = ndm * ddm
        if dm_range > 500:
            _LOG.error(
                "ERROR: Large DM range to handle, DM range %s = ", dm_range)
            raise FailReply(f"Large DM range to handle, DM range = {dm_range}")

    def script_stdout_parser(self, line: str):
        """Function search for an output of pattern and catches the plot from DMT_plot.py"""
        if "NEW PLOT: " in line:
            _LOG.debug("Got new plot, overwriting sensor with new b64 string")
            b64 = line.split(":")[1]
            self._png_plot.set_value(b64)
        else:
            _LOG.warning("Couldn't interpret stdout line")


def main():
    """The main function wrapper
    """
    asyncio.run(launchPipelineServer(TransientSearch))


if __name__ == "__main__":
    main()
