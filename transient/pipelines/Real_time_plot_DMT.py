from mpikat.pipelines.gated_spectrometer.plotter import encodeb64_figure
import matplotlib.pyplot as plt
import os
import time
import glob
import argparse
import numpy as np
import matplotlib
matplotlib.use('Agg')  # Use 'Agg' backend for non-interactive plotting


def find_latest_file(pattern):
    """Finds the most recently modified file matching the pattern."""
    files = glob.glob(pattern)
    if not files:
        return None
    # Get the most recently modified file
    latest_file = max(files, key=os.path.getmtime)
    return latest_file


def wait_for_file(pattern, check_interval=1):
    """Waits until a file matching the pattern appears."""
    print(f"Waiting for file matching pattern '{pattern}'...")
    while True:
        latest_file = find_latest_file(pattern)
        if latest_file:
            print(f"Found file: {latest_file}")
            return latest_file
        time.sleep(check_interval)


def follow(file):
    """Generator function that yields new lines from a file as it is written."""
    file.seek(0, os.SEEK_END)  # Move to the end of the file
    while True:
        line = file.readline()
        if not line:
            time.sleep(0.1)  # Wait briefly before trying again
            continue
        yield line


def parse_line(line):
    """Parses a line and returns timestamp, DM, and S/N."""
    columns = line.strip().split('\t')
    if len(columns) >= 5:
        try:
            timestamp = float(columns[2])  # 3rd column (zero-based index 2)
            dm = float(columns[3])        # 4th column (index 3)
            snr = float(columns[4])       # 5th column (index 4)
            return timestamp, dm, snr
        except ValueError:
            pass  # Handle lines that don't contain valid floats
    return None, None, None


def create_plot(timestamps, dms, snrs):
    """Creates and saves a scatter plot."""
    fig, ax = plt.subplots(figsize=(10, 6))

    # Scatter plot on specified axes
    ax.scatter(timestamps, dms, s=np.array(snrs) * 50,
               c='blue', alpha=0.6, edgecolors='none')
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('DM')
    ax.set_title('Real-Time Pulse Data')
    ax.grid(True)

    # Adjust x-axis to show the last 10 seconds
    if timestamps:
        current_time = timestamps[-1]
        # Adding 1 second buffer
        ax.set_xlim(max(current_time - 10, 0), current_time + 1)

    # Adjust y-axis dynamically
    if dms:
        ax.set_ylim(min(dms) - 1, max(dms) + 1)

    fig.tight_layout()
    b64 = encodeb64_figure(fig)

    print(f"NEW PLOT:{b64}")


def main():
    """Main function"""
    parser = argparse.ArgumentParser(
        description='Real-Time Pulse Data Plot and Save PNGs')
    parser.add_argument('--directory', default='.',
                        help='Directory to search for data files')
    parser.add_argument('--prefix', default='CAND',
                        help='Prefix to search for in data files (default: CAND)')
    parser.add_argument('--save_interval', type=int, default=10,
                        help='Interval in seconds to save PNG files')
    args = parser.parse_args()

    # Pattern to search for files starting with 'CAND?' and ending with '.cands'
    pattern = os.path.join(args.directory, '{args.prefix}*.cands')

    # Wait for the file to appear
    data_file = wait_for_file(pattern)

    # Initialize data lists
    timestamps = []
    dms = []
    snrs = []

    # Open the data file and start following it
    with open(data_file, 'r', encoding="utf-8") as file:
        loglines = follow(file)
        start_time = None
        last_save_time = time.time()

        for line in loglines:
            timestamp, dm, snr = parse_line(line)
            if timestamp is None:
                continue  # Skip invalid lines

            if start_time is None:
                start_time = timestamp

            # Calculate relative time in seconds
            relative_time = (timestamp - start_time) * 24 * \
                3600  # Convert days to seconds

            # Append data
            timestamps.append(relative_time)
            dms.append(dm)
            snrs.append(snr)

            # Keep only the last 100 data points
            if len(timestamps) > 100:
                timestamps.pop(0)
                dms.pop(0)
                snrs.pop(0)

            # Check if it's time to save a PNG
            current_time = time.time()
            if current_time - last_save_time >= args.save_interval:
                create_plot(timestamps, dms, snrs)
                last_save_time = current_time


if __name__ == '__main__':
    main()
