"""
Archiver class - Compresses individual files as they are written.
Includes a final compression pass upon stopping to handle any remaining files.
"""
import os
import threading
import time
import glob
import gzip
import tarfile
import shutil
import mpikat.core.logger as logging

_LOG = logging.getLogger("mpikat.transient.archiver")

class Archiver:
    def __init__(self, directory, prefix, interval=10, stability_threshold=5):
        """
        Initializes the Archiver.

        Args:
            directory (str): Directory to monitor for files.
            prefix (str): Prefix of files to archive (e.g., 'CAND').
            interval (int): Time interval in seconds between archiving operations.
            stability_threshold (int): Minimum time in seconds since the last modification
                                       of a file to consider it stable for archiving.
        """
        self.directory = directory
        self.prefix = prefix
        self.interval = interval
        self.stability_threshold = stability_threshold
        # Determine tarball name based on the directory name
        directory_name = os.path.basename(os.path.normpath(directory))
        tarball_name = f"{directory_name}.tar"

        self.tarball_path = os.path.join(self.directory, tarball_name)
        self.stop_event = threading.Event()
        self.thread = threading.Thread(target=self._archive_loop, daemon=True)
        self.lock = threading.Lock()  # To synchronize access to the tarball
        self.tar = None  # Tarfile object

    def start(self):
        _LOG.info("Starting Archiver thread.")
        self.tar = tarfile.open(self.tarball_path, mode='w')
        _LOG.info(f"Tarball opened: {self.tar}")
        self.thread.start()

    def stop(self):
        """
        Signals the archiving thread to stop and waits for it to finish.
        Then performs a final compression pass to compress any remaining files.
        """
        _LOG.info("Stopping Archiver thread.")
        self.stop_event.set()
        self.thread.join()
        _LOG.info("Archiver thread stopped.")
        # Perform final compression pass
        self._final_compression_pass()
        # Close the tarball
        self._finalize_tarball()

    def _archive_loop(self):
        """
        The main loop that performs archiving at regular intervals.
        """
        pattern = os.path.join(self.directory, f'{self.prefix}*.cands')
        _LOG.debug("Archiving pattern set to: %s", pattern)

        while not self.stop_event.is_set():
            self._compress_and_archive_files(pattern)
            # Sleep for the specified interval before next archiving attempt
            self.stop_event.wait(self.interval)

    def _compress_and_archive_files(self, pattern, final_pass=False):
        """
        Compresses files matching the pattern that meet the stability threshold.
        If final_pass is True, compresses all matching files regardless of stability.
        """
        try:
            matching_files = glob.glob(pattern)
            _LOG.debug("Found %d file(s) matching pattern.", len(matching_files))

            if final_pass:
                files_to_archive = matching_files
                _LOG.debug("Final pass: compressing all matching files.")
            else:
                current_time = time.time()
                files_to_archive = [
                    f for f in matching_files
                    if (current_time - os.path.getmtime(f)) > self.stability_threshold
                ]
                _LOG.debug("Compressing %d file(s) after applying stability threshold.", len(files_to_archive))

            if files_to_archive:
                _LOG.info("Compressing %d file(s).", len(files_to_archive))

                for file in files_to_archive:
                    try:
                        compressed_file = file + '.gz'
                        with open(file, 'rb') as f_in, gzip.open(compressed_file, 'wb') as f_out:
                            shutil.copyfileobj(f_in, f_out)
                        os.remove(file)
                        _LOG.info('Compressed and removed: %s', file)
                        with self.lock:
                            self.tar.add(compressed_file, arcname=os.path.basename(compressed_file))
                        os.remove(compressed_file)
                        _LOG.info('Added to tarball and removed compressed file: %s', compressed_file)
                    except Exception as file_e:
                        _LOG.warning("Failed to compress/remove %s: %s", file, file_e)
            else:
                _LOG.debug('No files to compress at this time.')
        except Exception as e:
            _LOG.warning('Error during archiving process: %s', e)

    def _final_compression_pass(self):
        """
        Performs a final compression pass to compress any remaining uncompressed files.
        """
        _LOG.info("Performing final compression pass.")
        pattern = os.path.join(self.directory, f'{self.prefix}*.cands')
        self._compress_and_archive_files(pattern, final_pass=True)
        _LOG.info("Final compression pass completed.")
        
    def _finalize_tarball(self):
        """
        Closes the tarball.
        """
        _LOG.info("Finalizing tarball.")
        with self.lock:
            if self.tar is not None:
                self.tar.close()
                self.tar = None
                _LOG.info("Tarball closed.")
